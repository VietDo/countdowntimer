/** Variables */

var isPlaying = false;
var isGoodFinish = true;

var defaultTheme = 'audio/theme/default.ogg';
var defaultAlarm = 'audio/alarm/cuckoo.ogg'

//Sound theme
var themesound = new Howl({
  urls: [defaultTheme],
  loop: true,
  volume: 0.1,
  onplay: function(){
	  isPlaying = true;
  }
});

//Alarm sound
var alarm = new Howl({
	urls:[defaultAlarm],
	loop: true
});

/** Functions */
// Functions for time editing
function getTimeRemaining(endtime){
	  var t = Date.parse(endtime) - Date.parse(new Date());
	  var seconds = t/1000; //Math.floor( (t/1000) % 60 );
	  return seconds;
}

function customTime(h, m, s){
	var deadline = new Date();
	deadline.setHours(h);
	deadline.setMinutes(m);
	deadline.setSeconds(s);
	return deadline;
}

$(document).ready(function(){
	$(document).tooltip({
		track: true,
		content: function(callback) { 
			callback($(this).prop('title').replace('|', '<br />')); 
		}
	});
	
	$("body").show();
	/**
	 * Music settings
	 * music files, music switch
	*/
	//Music switch
	var musicSwitch = $("input#mymusicswitch");
	musicSwitch.prop("checked", true);
	
	$("div.musicswitch").click(function(e){
		musicSwitch.prop("checked", !musicSwitch.prop("checked"));
	});
	
	/**
	* Time settings
	*/
	var nowTime = new Date();
	var hour = $("#hour").spinner({
		numberFormat: "d2",
		spin: function( event, ui ) {
			if ( ui.value > 23 ) {
			  $( this ).spinner( "value", 0 );
			  return false;
			} else if ( ui.value < 0 ) {
			  $( this ).spinner( "value", 23 );
			  return false;
			}
		}
	});
	
	var min = $("#min").spinner({
		numberFormat: "d2",
		spin: function( event, ui ) {
			if ( ui.value > 59 ) {
				$( this ).spinner( "value", 0 );
				hour.spinner("stepUp");
				return false;
			} else if ( ui.value < 0 ) {
				$( this ).spinner( "value", 59 );
				hour.spinner("stepDown");
				return false;
			}
	  }
	});
	
	var sec = $("#sec").spinner({
		numberFormat: "d2",
		spin: function( event, ui ) {
			if ( ui.value > 59 ) {
			  $( this ).spinner( "value", 0 );
			  min.spinner("stepUp");
			  return false;
			} else if ( ui.value < 0 ) {
			  $( this ).spinner( "value", 59 );
			  hour.spinner("stepDown");
			  return false;
			}
		  }
	});
	
	hour.spinner("value", nowTime.getHours());
	min.spinner("value", nowTime.getMinutes());
	sec.spinner("value", nowTime.getSeconds());
	
	//Dialog announce end timer
	var dialogEnd = $("#announceDialog").dialog({
		autoOpen: false,
		resizable:false,
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function(){
			$("#clockCount").hide();
			$("#formInit").show();
			alarm.stop();
		}
	});
	
	//Flip clock
	var clock = $('.clock').FlipClock({
		countdown: true,
		autoStart: false,
		callbacks:{
			stop: function(){
				if(isGoodFinish){
					window.focus();
					dialogEnd.dialog("open");
					//Stop theme sound
					themesound.stop();
					isPlaying = false;
					alarm.play();
				}
			}
		}
	});
	
	//Restart button
	var resetButton = $("#resetClock").button()
	.css("opacity", "0.3")
	.mouseenter(function(){
		$(this).fadeTo("fast", 1);
	})
	.mouseleave(function(){
		$(this).fadeTo("slow", 0.3)
	})
	.click(function(e){
		e.preventDefault;
		$("#clockCount").hide();
		$("#formInit").show();
		//Stop music
		themesound.stop();
		isGoodFinish = false;
		clock.stop();
		isGoodFinish = true;
	});
	
	//Toggle theme sound
	var toggleSound = $("#toggle-theme-sound")
	.click(function(e){
		if(isPlaying){
			themesound.stop();
			isPlaying = false;
			$(this).find(".ui-icon").removeClass("ui-icon-volume-on").addClass("ui-icon-volume-off");
		}
		else{
			themesound.play();
			$(this).find(".ui-icon").removeClass("ui-icon-volume-off").addClass("ui-icon-volume-on");
		}
	});
	
	
	/** 
	* Start button
	* Config all time to timer FlipClock
	*/
	var startButton = $("#startClock").button()
	.click(function(e){
		e.preventDefault();
		var htime = hour.spinner("value");
		var mtime = min.spinner("value");
		var stime = sec.spinner("value");
		
		var endTime = customTime(htime,mtime,stime);
		var time = getTimeRemaining(endTime);
		var timeText = $("#text-announce").val();
		var errorDiv = $("#error-mess");
		
		errorDiv.empty();
						
		if(time > 0 && timeText){
			clock.setTime(time);
			clock.start();
			
			//Start theme sound
			if(musicSwitch.prop("checked")){
				themesound.play();
			}
			else{
				toggleSound.find(".ui-icon").removeClass("ui-icon-volume-on").addClass("ui-icon-volume-off");
			}
				
			$("#text-mess").text(timeText);
			$("#announceDialog-text").text(timeText);
			$("#clockCount").show();
			$("#error-mess").hide();
			$("#formInit").hide();
			$("#text-announce").removeClass("error-input");
			$("#endtime").removeClass("error-input-text");
			errorDiv.hide();
		}
		else{
			if(time <= 0){
				var now = new Date();
				var nowText = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
				var errMess = "Cannot count down to the past! ";
				var errHelp = "It's already " + nowText;
				$("#endtime").addClass("error-input-text");
				$("#endtime").prop("title", errMess + "|" + errHelp);
			}
			else
				$("#endtime").removeClass("error-input-text")

			if(!timeText){
				var errMess = "Set time to do something..."
				$("#text-announce").prop('title', errMess);
				$("#text-announce").addClass("error-input");
			}
			else
				$("#text-announce").removeClass("error-input");
			errorDiv.append("<span class='error'> Invalid input <span>");
			errorDiv.show();
		}
	});
	
});